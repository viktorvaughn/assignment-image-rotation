#ifndef BMPROTATE_FROM_TO_BMP_H
#define BMPROTATE_FROM_TO_BMP_H

#include <stdio.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdlib.h>
#include <stdbool.h>
#include "image.h"

/*  deserializer   */
enum read_status {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER
};

enum read_status from_bmp(FILE *in, struct image *img);

/*  serializer   */
enum write_status {
    WRITE_OK = 0,
    WRITE_ERROR
};

enum write_status to_bmp(FILE *out, struct image const *img);

struct image read_bmp(const char *filename, FILE *file);

enum write_status write_bmp(FILE *file, struct image const *image);

#endif