#include "from_to_bmp.h"

#include <stdio.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdlib.h>
#include <stdbool.h>

#define BM 19778

#define PRI_SPECIFIER(e) (_Generic( (e), uint16_t : "%" PRIu16, uint32_t: "%" PRIu32, default: "NOT IMPLEMENTED" ))

#define PRINT_FIELD(t, name) \
    fprintf( f, "%-17s: ",  # name ); \
    fprintf( f, PRI_SPECIFIER( header-> name ) , header-> name );\
    fprintf( f, "\n");

#define FOR_BMP_HEADER(FOR_FIELD) \
        FOR_FIELD( uint16_t,bfType)\
        FOR_FIELD( uint32_t,bfileSize)\
        FOR_FIELD( uint32_t,bfReserved)\
        FOR_FIELD( uint32_t,bOffBits)\
        FOR_FIELD( uint32_t,biSize)\
        FOR_FIELD( uint32_t,biWidth)\
        FOR_FIELD( uint32_t,biHeight)\
        FOR_FIELD( uint16_t,biPlanes)\
        FOR_FIELD( uint16_t,biBitCount)\
        FOR_FIELD( uint32_t,biCompression)\
        FOR_FIELD( uint32_t,biSizeImage)\
        FOR_FIELD( uint32_t,biXPelsPerMeter)\
        FOR_FIELD( uint32_t,biYPelsPerMeter)\
        FOR_FIELD( uint32_t,biClrUsed)\
        FOR_FIELD( uint32_t,biClrImportant)

#define DECLARE_FIELD(t, n) t n ;

struct __attribute__((packed)) bmp_header {
    FOR_BMP_HEADER(DECLARE_FIELD)
};

void bmp_header_print(struct bmp_header const *header, FILE *f) {
    FOR_BMP_HEADER(PRINT_FIELD)
}

bool read_header_from_file(const char *filename, struct bmp_header *header) {
    if (!filename) return false;
    FILE *f = fopen(filename, "rb");
    if (!f) return false;
    if (fread(header, sizeof(struct bmp_header), 1, f)) {
        fclose(f);
        return true;
    }
    fclose(f);
    return false;
}


static enum read_status read_bmp_header(FILE *in, struct bmp_header *header) {
    if (!fread(header, sizeof(struct bmp_header), 1, in)) {
        return false;
    }
    if (header->biSize != 40 || header->bfReserved != 0 || (header->biWidth == 0 && header->biHeight == 0)) {
        return READ_INVALID_HEADER;
    }
    if (header->biBitCount != 24) {
        return READ_INVALID_BITS;
    }
    if (header->bfType != BM) {
        return READ_INVALID_SIGNATURE;
    }
    return READ_OK;
}

static size_t padding(const size_t width) {
    if (((width * sizeof(struct pixel)) % 4) != 0) return (4 - ((width * sizeof(struct pixel)) % 4));
    return 0;
}

enum read_status from_bmp(FILE *in, struct image *img) {
    struct bmp_header header = {0};
    if (read_bmp_header(in, &header) != READ_OK) {
        printf("Failed to read header \n");
    }
    const size_t pad = padding(img->width);
    fseek(in, header.bOffBits, SEEK_SET);
    img->data = calloc(img->width * img->height, sizeof(struct pixel));
    for (size_t col = 0; col < img->height; col++) {
        if (fread(img->data + col * img->width, sizeof(struct pixel), img->width, in) != img->width)
            return READ_INVALID_BITS;
        if (fseek(in, pad, SEEK_CUR) != 0) return READ_INVALID_BITS;
    }
    return READ_OK;
}

enum write_status to_bmp(FILE *out, struct image const *img) {
    size_t k = 0;
    const size_t pad = padding(img->width);

    for (size_t col = 0; col < img->height; col++) {
        k += fwrite(img->data + col * img->width, sizeof(struct pixel), img->width, out);
        k += fwrite("0", sizeof(uint8_t), pad, out);
    }

    if (k != ((img->width + pad) * img->height)) return WRITE_ERROR;
    return WRITE_OK;
}

struct image read_bmp(const char *filename, FILE *file) {
    struct bmp_header header = {0};
    if (read_header_from_file(filename, &header)) {
        bmp_header_print(&header, stdout);
    } else {
        printf("Failed to open BMP file or reading header \n");
    }
    return create_image(header.biWidth, header.biHeight, file);
}

struct bmp_header create_header(struct image const *image) {
    struct bmp_header header = {
            .bfType = BM,
            .bfileSize = (sizeof(struct pixel) * image->width + image->width % 4) * image->height +
                         sizeof(struct bmp_header),
            .bfReserved = 0,
            .bOffBits = sizeof(struct bmp_header),
            .biSize = 40,
            .biWidth = image->width,
            .biHeight = image->height,
            .biPlanes = 1,
            .biBitCount = 24,
            .biCompression = 0,
            .biSizeImage = header.bfileSize - header.bOffBits,
            .biXPelsPerMeter = 0,
            .biYPelsPerMeter = 0,
            .biClrUsed = 0,
            .biClrImportant = 0
    };
    return header;
}


enum write_status write_bmp(FILE *file, struct image const *image) {
    struct bmp_header header = create_header(image);
    fwrite(&header, sizeof(struct bmp_header), 1, file);
    if (to_bmp(file, image) != READ_OK) {
        printf("Failed to write file \n");
        return WRITE_ERROR;
    }
    return WRITE_OK;
}