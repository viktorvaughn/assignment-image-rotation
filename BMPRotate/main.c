#include <stdio.h>

#include "image.h"
#include "from_to_bmp.h"
#include "rotate.h"

void usage() {
    fprintf(stderr, "Usage: ./BMPRotate BMP_FILE_NAME\n");
    exit(1);
}

int main(int argc, char **argv) {
    if (argc != 2) usage();
    if (argc < 2) printf("Not enough arguments \n");
    if (argc > 2) printf("Too many arguments \n");

    FILE *in;
    if ((in = fopen(argv[1], "rb")) != NULL) {
        struct image image = read_bmp(argv[1], in);
        image = rotate(image);
        fclose(in);
        FILE *out;
        if ((out = fopen("C:\\Users\\Home\\CLionProjects\\BMPRotate\\Images\\new_low.bmp", "wb")) != NULL) {
            write_bmp(out, &image);
            fclose(out);
            printf("Rotated image file has been created \n");
        } else {
            printf("Failed to open file \n");
        }
    } else {
        printf("Failed to open file \n");
    }
    return 0;
}