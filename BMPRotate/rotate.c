#include "rotate.h"

struct image rotate(struct image const source) {
    struct image rotated;
    rotated.height = source.width;
    rotated.width = source.height;
    rotated.data = malloc(source.width * source.height * sizeof(struct pixel));

    for (size_t row = 0; row < source.width; row++) {
        for (size_t col = 0; col < source.height; col++) {
            rotated.data[((source.height - col - 1) * source.width) + row] = source.data[row * source.height + col];
        }
    }
    return rotated;
}
