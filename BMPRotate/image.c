#include "image.h"
#include "from_to_bmp.h"

#include <stdio.h>

struct image create_image(const size_t width, const size_t height, FILE *file) {
    struct image image;
    image.width = width;
    image.height = height;
    if (from_bmp(file, &image) != READ_OK) {
        printf("Failed to read the file \n");
    }
    return image;
}
