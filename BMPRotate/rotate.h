#ifndef BMPROTATE_ROTATE_H
#define BMPROTATE_ROTATE_H

#include <stdio.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdlib.h>
#include "image.h"

struct image rotate(struct image source);

#endif //BMPROTATE_ROTATE_H