#ifndef BMPROTATE_IMAGE_H
#define BMPROTATE_IMAGE_H

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

struct pixel {
    uint8_t b, g, r;
};

struct image {
    uint64_t width, height;
    struct pixel *data;
};

struct image create_image(size_t width, size_t height, FILE *file);

#endif
